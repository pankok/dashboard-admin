import { NewUser } from './../interfaces/new-user';
import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { defer, Observable } from 'rxjs';


const BASE_URL = "http://localhost:8080/login"

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  constructor(private httpCliente:HttpClient) { }

  login(email:String, password:String):Observable<any>{
    // return this.httpCliente.post(BASE_URL,{
    //   email:email,
    //   password:password
    // })
    var promise:Observable<any> = Observable.create((obs)=>{
        setTimeout(() => {
          obs.next(
            {user:email, key:'dsadkhjgasj'}
            )
        }, 1000);
    })
    
    return  promise;
  }

  register(user:NewUser):Observable<any>{
    
    var promise:Observable<any> = Observable.create((obs)=>{
        setTimeout(() => {
          obs.next(
            {user:user.userName, email:user.email , key:'dsadkhjgasj'}
            )
        }, 1500);
    })
    
    return  promise;
  }
}
