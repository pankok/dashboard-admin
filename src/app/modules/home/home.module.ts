import { MessageModule } from './../../components/message/message.module';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { HomeComponent } from './home.component';
import { PhotoComponent } from './components/photo/photo.component';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [
      LoginComponent,
      RegisterComponent,
      HomeComponent,
      PhotoComponent
    ],
  imports: [
    CommonModule,
    HomeRoutingModule,
    FormsModule,
    MessageModule
  ],
  exports: [HomeComponent]
})
export class HomeModule { }
