import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from 'src/app/modules/home/services/authentication.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  email=''
  password=''

  constructor(private authService:AuthenticationService,private router:Router) { }

  ngOnInit(): void {
  }

  login(){
    var data = this.authService.login(this.email,this.password).subscribe(
        (sd)=>{
          console.log(sd);
          this.router.navigate(['forum'])
        }
        
      );
    
    
  }

}
